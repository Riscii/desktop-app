import {task, src, dest} from './helpers/gulp';
import del   from 'del';
import paths from './paths.js';

task('npm:clean', ()=> {
  return del(`${paths.nodeModules}`);
});

task('jspm:clean', ()=> {
  return del(`${paths.jspmPackages}`);
});

task('clean', [
  'debug:clean',
  'release:clean',
  'npm:clean',
  'jspm:clean'
]);

task('default', ()=> {

});
