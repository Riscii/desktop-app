import {task, src, dest} from './helpers/gulp';
import path       from 'path';
import paths      from './paths';
import spawn      from './helpers/spawn';
import del        from 'del';
import packager   from 'electron-packager';
import jspm       from 'jspm';
import sass       from 'gulp-sass'; 
import babel      from 'gulp-babel';
import rename     from 'gulp-rename';
import preprocess from 'gulp-preprocess';
import insert     from 'gulp-insert';

// used for preprocess context
const CONFIG = {
  ENV:     'release',
  APPNAME: 'FarmLogs'
};

/*
  CLEAN
*/

// CLIENT CLEAN
task('release:clean:client:babel', (cb)=> {
  return del(`${paths.release}/_tmp_babel`, cb);
});

task('release:clean:client:jspm', (cb)=> {
  return del(`${paths.release}/package/client/index.js`, cb);
});

task('release:clean:client:html', (cb)=> {
  return del(`${paths.release}/package/client/**/*.html`, cb);
});

task('release:clean:client:sass', (cb)=> {
  return del(`${paths.release}/package/client/**/*.css`, cb);
});

task('release:clean:client:assets', (cb)=> {
  return del(`${paths.release}/package/client/assets/**/*`, cb);
});

// DESKTOP CLEAN
task('release:clean:desktop:babel', (cb)=> {
  return del([`${paths.release}/package/**/*.js`, `!${paths.release}/package/client/**/*.js`], cb);
});

task('release:clean:desktop:json', (cb)=> {
  return del([`${paths.release}/package/**/*.json`, `!${paths.release}/package/client/**/*.json`], cb);
});

// ALL CLEAN
task('release:clean', (cb)=> {
  return del(`${paths.release}`, cb);
});


/*
  BUILD CLIENT
*/

// BABEL CLIENT
task('release:build:client:babel', [
  'release:clean:client:babel'
], ()=> {
  return src([`${paths.client}/**/*.jsx`, `${paths.client}/**/*.js`])
    .pipe(preprocess({context: CONFIG}))

    // Implicit import of react on all JSX files
    .pipe(insert.transform((contents, file)=> {
      if(path.extname(file.path) === '.jsx') {
        contents = 'import React from \'react\';\n' + contents;
      }
      return contents;
    }))

    // rename .jsx files to .jsx.js so that jspm will link
    .pipe(rename((path)=> {
      if(path.extname === ".jsx") {
        path.extname += ".js";
      }
    }))

    .pipe(babel({
      presets: ['es2015', 'react']
    }))
    .pipe(dest(`${paths.release}/_tmp_babel/`));
});

// JSPM CLIENT
task('release:build:client:jspm', [
  'release:build:client:babel',
  'release:clean:client:jspm'
], (cb)=> {
  var moduleName = `${paths.release}/_tmp_babel/app/index.js`;
  var fileName = `${paths.release}/package/client/index.js`;
  var options = {};

  jspm.bundleSFX(moduleName, fileName, options)
    .then(()=> {
      cb();
    })
    .catch(()=> {
      cb();
    });
});

// HTML CLIENT
task('release:build:client:html', [
  'release:clean:client:html'
], ()=> {
  return src(`${paths.client}/**/*.html`)
    .pipe(preprocess({context: CONFIG}))
    .pipe(dest(`${paths.release}/package/client/`));
});

// SCSS CLIENT
task('release:build:client:sass', [
  'release:clean:client:sass'
], ()=> {
  return src(`${paths.client}/styles/index.scss`)
    .pipe(sass({
      includePaths: [`${paths.nodeModules}/bootstrap/scss/`]
    })
    .on('error', sass.logError))
    .pipe(dest(`${paths.release}/package/client/`));
});

// ASSETS CLIENT
task('release:build:client:assets', [
  'release:clean:client:assets'
], ()=> {
  return src(`${paths.client}/assets/**/*`)
    .pipe(dest(`${paths.release}/package/client/assets/`));
});

// ALL CLIENT
task('release:build:client', [
  'release:build:client:babel',
  'release:build:client:jspm',
  'release:build:client:html',
  'release:build:client:sass',
  'release:build:client:assets'
]);


/*
  BUILD DESKTOP
*/

// BABEL DESKTOP
task('release:build:desktop:babel', [
  'release:clean:desktop:babel'
],()=> {
  return src(`${paths.desktop}/**/*.js`)
    .pipe(preprocess({context: CONFIG}))
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(dest(`${paths.release}/package/`));
});

// JSON DESKTOP
task('release:build:desktop:json', [
  'release:clean:desktop:json'
],()=> {
  return src(`${paths.desktop}/**/*.json`)
    .pipe(dest(`${paths.release}/package/`));
});

// ALL DESKTOP
task('release:build:desktop', [
  'release:build:desktop:babel',
  'release:build:desktop:json'
]);

/*
  ELECTRON
*/
task('release:build:electron', [
  'release:build:client',
  'release:build:desktop'
], (cb)=> {
  packager({
    overwrite: true,
    dir:       `${paths.release}/package/`,
    name:      CONFIG.APPNAME,
    platform:  'darwin',
    version:   '0.36.1',
    arch:      'x64',
    icon:      `${paths.release}/package/client/assets/icon.icns`,
    out:       `${paths.release}/out`
  }, (err, appPath)=> {
    cb();
  });
});

/*
  ALL
*/
task('release:build', [
  'release:build:client',
  'release:build:desktop',
  'release:build:electron'
]);
