import gulp from 'gulp';

export var task  = (...args)=> gulp.task(...args);
export var src   = (...args)=> gulp.src(...args);
export var dest  = (...args)=> gulp.dest(...args);
export var watch = (...args)=> gulp.watch(...args);
