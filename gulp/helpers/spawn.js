import gutil   from 'gulp-util';
import {spawn} from 'child_process';

export default function(path, args, cb) {
  return new Promise((resolve, reject)=> {
    var ls = spawn(path, args);

    ls.stdout.on('data', (data)=> {
      gutil.log(gutil.colors.yellow(data));
    });

    ls.stderr.on('data', (data)=> {
      gutil.log(gutil.colors.red(data));
    });

    ls.on('error', (err)=> {
      process.stdout.write('\n');
      process.stdout.write(gutil.colors.red(err.stack));
      process.stdout.write('\n\n');
      reject(err);
    });

    ls.on('close', (code)=> {
      gutil.log(gutil.colors.yellow(`App exited with code ${code}`));
      resolve(code);
    });

    gutil.log(gutil.colors.yellow(`App started with arguments `, args.join(', ')));
  });
};
