export default {
    client:       `${process.cwd()}/client`,
    desktop:      `${process.cwd()}/desktop`,
    debug:        `${process.cwd()}/_debug`,
    release:      `${process.cwd()}/_release`,
    nodeModules:  `${process.cwd()}/node_modules`,
    nodeBin:      `${process.cwd()}/node_modules/.bin`,
    jspmPackages: `${process.cwd()}/jspm_packages` 
};
