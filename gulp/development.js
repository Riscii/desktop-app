import {task, src, dest} from './helpers/gulp';
import gulp       from 'gulp';
import paths      from './paths';
import spawn      from './helpers/spawn';
import fs         from 'fs';
import del        from 'del';
import connect    from 'gulp-connect';
import sourcemaps from 'gulp-sourcemaps';
import sass       from 'gulp-sass';
import watch      from 'gulp-watch';
import babel      from 'gulp-babel';
import insert     from 'gulp-insert';
import rename     from 'gulp-rename';
import preprocess from 'gulp-preprocess';

// used for preprocess context
const CONFIG = {
  ENV:     'debug',
  APPNAME: 'FarmLogs'
};

/*
  CLEAN
*/

// CLIENT CLEAN
task('debug:clean:client:js', ()=> {
  return del([
    `${paths.debug}/client/**/*.js`,
    `${paths.debug}/client/**/*.js.map`,
    `!${paths.debug}/client/**/*.jsx.js`,
    `!${paths.debug}/client/jspm_packages/**/*`,
    `!${paths.debug}/client/config.js`
  ]);
});

task('debug:clean:client:jsx', ()=> {
  return del([
    `${paths.debug}/client/**/*.jsx.js`,
    `${paths.debug}/client/**/*.jsx.js.map`,
    `!${paths.debug}/client/jspm_packages/**/*`
  ]);
});

task('debug:clean:client:html', ()=> {
  return del([
    `${paths.debug}/client/**/*.html`,
    `!${paths.debug}/client/jspm_packages/**/*`
  ]);
});

task('debug:clean:client:sass', ()=> {
  return del([
    `${paths.debug}/client/**/*.css`,
    `${paths.debug}/client/**/*.css.map`,
    `!${paths.debug}/client/jspm_packages/**/*`
  ]);
});

task('debug:clean:client:jspm', ()=> {
  return del([
    `${paths.debug}/client/jspm_packages`,
    `${paths.debug}/client/config.js`
  ]);
});

// DESKTOP CLEAN
task('debug:clean:desktop:babel', ()=> {
  return del(`${paths.debug}/desktop/**/*.js`);
});

task('debug:clean:desktop:json', ()=> {
  return del(`${paths.debug}/desktop/**/*.json`);
});

// ALL CLEAN
task('debug:clean', ()=> {
  return del(`${paths.debug}`);
});


/*
  WATCH CLIENT
*/


// JS CLIENT
task('debug:watch:client:js', [
  'debug:clean:client:js'
], (cb)=> {
  return src(`${paths.client}/**/*.js`)
    .pipe(watch(`${paths.client}/**/*.js`))
    .on('ready', ()=> { cb(); })
    .on('unlink', (file)=> {
      var dstPath = file.replace(paths.client, `${paths.debug}/client/`);
      del(`${dstPath}`);
      del(`${dstPath}.map`);
    })
    .pipe(sourcemaps.init())
    .pipe(preprocess({context: CONFIG}))
    .pipe(babel({
      presets: ['es2015', 'react']
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(connect.reload())
    .pipe(dest(`${paths.debug}/client/`));
});


// JSX CLIENT
task('debug:watch:client:jsx', [
  'debug:clean:client:jsx'
], (cb)=> {
  return src(`${paths.client}/**/*.jsx`)
    .pipe(watch(`${paths.client}/**/*.jsx`))
    .on('ready', ()=> { cb(); })
    .on('unlink', (file)=> {
      var dstPath = file.replace(paths.client, `${paths.debug}/client/`);
      del(`${dstPath}.js`);
      del(`${dstPath}.js.map`);
    })
    // Implicit import of react on all JSX files
    .pipe(insert.prepend('import React from \'react\';\n'))
    .pipe(sourcemaps.init())
    .pipe(preprocess({context: CONFIG}))
    .pipe(babel({
      presets: ['es2015', 'react']
    }))
    .pipe(rename({extname: '.jsx.js'}))
    .pipe(sourcemaps.write('.'))
    .pipe(connect.reload())
    .pipe(dest(`${paths.debug}/client/`));
});


// HTML CLIENT
task('debug:watch:client:html', [
  'debug:clean:client:html'
], (cb)=> {
  src(`${paths.client}/**/*.html`)
    .pipe(preprocess({context: CONFIG}))
    .pipe(sourcemaps.init())
    .pipe(watch(`${paths.client}/**/*.html`))
    .on('ready', ()=> { cb(); })
    .on('unlink', (file)=> {
      del(file.replace(paths.client, `${paths.debug}/client/`))
    })
    .pipe(connect.reload())
    .pipe(sourcemaps.write('.'))
    .pipe(dest(`${paths.debug}/client/`));
});


// SASS WATCH
task('debug:watch:client:sass', [
  'debug:build:client:sass'
], ()=> {
  gulp.watch(`${paths.client}/styles/**/*.scss`, ['debug:build:client:sass']);
});

// DESKTOP WATCH
task('debug:watch:desktop:babel', [
  'debug:clean:desktop:babel'
], (cb)=> {
  src(`${paths.desktop}/**/*.js`)
    .pipe(preprocess({context: CONFIG}))
    .pipe(watch(`${paths.desktop}/**/*.js`))
    .on('ready', ()=> { cb(); })
    .on('unlink', (file)=> {
      var dstPath = file.replace(paths.desktop, `${paths.debug}/desktop/`);
      del(`${dstPath}`);
      del(`${dstPath}.map`);
    })
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(dest(`${paths.debug}/desktop/`));
});

// JSON WATCH
task('debug:watch:desktop:json', [
  'debug:clean:desktop:json'
], (cb)=> {
  src(`${paths.desktop}/**/*.json`)
    .pipe(watch(`${paths.desktop}/**/*.json`))
    .on('ready', ()=> { cb(); })
    .on('unlink', (file)=> {
      del(file.replace(paths.desktop, `${paths.debug}/desktop/`))
    })
    .pipe(dest(`${paths.debug}/desktop/`));
});

// ALL WATCH
task('debug:watch', [
  'debug:watch:client:js',
  'debug:watch:client:jsx',
  'debug:watch:client:sass',
  'debug:watch:client:html',
  'debug:watch:desktop:babel',
  'debug:watch:desktop:json'
]);


// SASS CLIENT
task('debug:build:client:sass', [
  'debug:clean:client:sass'
], ()=> {
  return src(`${paths.client}/styles/index.scss`)
    .pipe(sourcemaps.init())
    .pipe(sass({
      includePaths: [`${paths.nodeModules}/bootstrap/scss/`]
    })
    .on('error', sass.logError))
    .pipe(sourcemaps.write('.'))
    .pipe(connect.reload())
    .pipe(dest(`${paths.debug}/client/`));
});


// LINK
task('debug:link:client:jspm', [
  'debug:clean:client:jspm'
], (cb)=> {
  fs.mkdir(`${paths.debug}`, ()=> {
    fs.mkdir(`${paths.debug}/client/`, ()=> {
      fs.symlink(`${paths.jspmPackages}`, `${paths.debug}/client/jspm_packages`, ()=> {
        fs.symlink(`${process.cwd()}/config.js`, `${paths.debug}/client/config.js`, cb);
      });
    });
  });
});


// ALL
task('develop', [
  'debug:watch',
  'debug:link:client:jspm'
], (cb)=> {
  connect.server({
    root: `${paths.debug}/client/`,
    livereload: true
  });

  spawn(`${paths.nodeBin}/electron`, [`${paths.debug}/desktop/`])
    .catch((e)=> { 
      // We might be in windows... try launching with....
      return spawn(`${paths.nodeBin}/electron.cmd`, [`${paths.debug}/desktop/`])
    })
    .catch((e)=> {
      console.log(e);
    })
    .then(()=> {
      connect.serverClose();

      setTimeout(()=> {
        cb();
      }, 250);

      setTimeout(()=> {
        process.exit(0);
      }, 500);
    });
});
