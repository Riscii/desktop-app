# FL Desktop Application
An example application using...

* Electron
* ES6
* React
* Bootstrap
 
## Getting Started
Install the project

`npm install`

Gulp is used for the build system.

`npm install gulp-cli -g`

Start development

`gulp develop`

Build a release

`gulp release:build`