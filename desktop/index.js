import {app, BrowserWindow} from 'electron';

var mainWindow = null;

// Quit when all windows are closed.
app.on('window-all-closed', ()=> {
  app.quit();
});

app.on('ready', ()=> {
  mainWindow = new BrowserWindow({
    width:              320,
    height:             450,
    center:             true,
    resizable:          false,
    backgroundColor:    '#45a15c',
    title:              '/* @echo APPNAME */',
    show:               false,
    autoHideMenuBar:    true,
    'node-integration': 'iframe',
    'webPreferences':   {'webSecurity': false}
  });

// @if ENV='debug'
  mainWindow.loadURL(`http://localhost:8080`);
// @endif

// @if ENV='release'
  mainWindow.loadURL(`file://${__dirname}/client/index.html`);
// @endif 

  mainWindow.on('closed', ()=> {
    mainWindow = null;
  });
});
