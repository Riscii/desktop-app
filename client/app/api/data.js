export function apiLogin(email, password) {
  email = email.replace(/\@/gi, "%40");

  var loginHeaders = new Headers();
  loginHeaders.append("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
  loginHeaders.append("X-Requested-With", "XMLHttpRequest");
  loginHeaders.append("X-Client-Name", "Web");


  return fetch("https://app.farmlogs.com/v1.8/users/login", {
    cache: 'no-cache',
    method: 'POST',
    headers: loginHeaders,
    body: `email=${email}&password=${password}`
  }).then((response)=> {
    return response.json();
  });
}


