class Data {
  constructor() {
    this.store = {};
  }

  set(key, data) {
    this.store[key] = data;
  }

  get(key) {
    return this.store[key];
  }
}

export default new Data();
