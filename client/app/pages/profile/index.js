import React      from 'react';
import profileJSX from './profile.jsx';
import router     from '../../router';

export default class extends React.Component {
  handleLogout() {
    router.login();
  }

  render() {
    return profileJSX.apply(this);
  }
}
