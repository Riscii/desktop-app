import SplashComponent from '../../components/splash/index';

export default function() {
  return (
    <div style={{marginTop: "50px", marginBottom: "50px"}} className="container container-fluid">
      <div className="row">
        <div className="col-sm-4 col-sm-offset-4">
          <SplashComponent style={{margin: "0 auto", height: "190px"}}/>
        </div>
      </div>
      <div className="row">
        <div className="col-md-6">
          <div className="card card-block">
            <h4 className="card-title">Contact Info</h4>
            <table style={{marginBottom: 0}} className="table">
              <tbody>
                <tr>
                  <th>Name</th>
                  <td>{this.props.name}</td>
                </tr>
                <tr>
                  <th>Email</th>
                  <td>{this.props.email}</td>
                </tr>
                <tr>
                  <th>Phone</th>
                  <td>{this.props.phone}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div className="col-md-6">
          <div className="card card-block">
            <h4 className="card-title">App Info</h4>
            <table style={{marginBottom: 0}} className="table">
              <tbody>
                <tr>
                  <th>Last Login</th>
                  <td>{this.props.last_login}</td>
                </tr>
                <tr>
                  <th>ID</th>
                  <td>{this.props.id}</td>
                </tr>
              </tbody>
            </table>
            <hr style={{marginTop:0, marginBottom: "14px"}}/>
            <button onClick={this.handleLogout} className="btn btn-primary btn-xs btn-block">Logout</button>
          </div>
        </div>
      </div>
    </div>
  );
};
