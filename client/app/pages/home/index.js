import React    from 'react';
import homeJSX from './home.jsx';

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isLoading: false};
  }

  render() {
    return homeJSX.apply(this);
  }
}
