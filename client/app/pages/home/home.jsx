import LoginComponent  from '../../components/login/index';
import SplashComponent from '../../components/splash/index';

export default function() {
  return (
    <div style={{height:"100%", alignItems: "center", justifyContent: "center", display: "flex", flexDirection: "column"}}>
      <div style={{height: "200px"}}>
        <SplashComponent />
      </div>
      <LoginComponent style={{width:"80%"}}/>
    </div>
  );
};
