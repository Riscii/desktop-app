import React      from 'react';
import loginJSX   from './login.jsx';
import                 'gsap-react-plugin';
import TweenMax   from 'greensock/tweenmax';
import {apiLogin} from  '../../api/data';
import data       from '../../data';
import router     from '../../router';

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      opacity: 0
    };
  }
  componentDidMount() {
    TweenMax.to(this, 0.5, {
      state: {opacity: 1},
      ease: Cubic.easeInOut
    });

    this.elEmailInput.focus();
  }

  handleSubmit(e) {
    e.preventDefault();

    apiLogin(
      this.elEmailInput.value,
      this.elPasswordInput.value
    ).then((res)=> {
      if(!(res.message && res.message === "Unauthorized")) {
        data.set('profile', res);
        router.profile();
      }
    });

  }

  render() {
    return loginJSX.apply(this);
  }
}