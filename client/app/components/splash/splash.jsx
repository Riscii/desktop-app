import {LeftLeaf, RightLeaf, FarmlogsVector} from './leaf';

const styleLeft = {
  position: "absolute",
  left: 0,
  bottom: 0,
  width: "44px"
};

const styleRight = {
  position: "absolute",
  right: 0,
  bottom: 0,
  width: "44px"
}

const styleLogo = {
  width: "100px",
  height: "150px",
  position: "relative",
  margin: "0 auto"
};

const styleLLeaf = {
  position: "absolute",
  transformOrigin: "bottom right"
};

const styleRLeaf = {
  position: "absolute",
  transformOrigin: "bottom left"
};

export default function() {
  return (
    <div {...this.props} style={Object.assign({}, this.props.style, { opacity: this.state.opacity})}>
      <div style={styleLogo}>
        <div style={styleLeft}>
          <LeftLeaf style={Object.assign({bottom: 90, opacity: this.state.lFiveOpacity, transform: `scale(${this.state.lFiveScale})`}, styleLLeaf)}/>
          <LeftLeaf style={Object.assign({bottom: 45, opacity: this.state.lThreeOpacity, transform: `scale(${this.state.lThreeScale})`}, styleLLeaf)} />
          <LeftLeaf style={Object.assign({bottom: 0, opacity: this.state.lOneOpacity, transform: `scale(${this.state.lOneScale})`}, styleLLeaf)} />
        </div>
        <div style={styleRight}>
          <RightLeaf style={Object.assign({bottom: 45, opacity: this.state.lFourOpacity, transform: `scale(${this.state.lFourScale})`}, styleRLeaf)} />
          <RightLeaf style={Object.assign({bottom: 0, opacity: this.state.lTwoOpacity, transform: `scale(${this.state.lTwoScale})`}, styleRLeaf)} />
        </div>
      </div>
    </div>
  );
};
