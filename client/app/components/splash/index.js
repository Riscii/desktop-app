import React     from 'react';
import ReactDOM  from 'react-dom';
import splashJSX from './splash.jsx';
import {leftLeafSVG, rightLeafSVG} from './leaf';
import                'gsap-react-plugin';
import TweenMax  from 'greensock/tweenmax';

const leafStateKeys = ['lOne', 'lTwo', 'lThree', 'lFour', 'lFive'];

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      opacity: 1
    };

    leafStateKeys.forEach((leafStateKey)=> {
      this.state[`${leafStateKey}Opacity`] = 0;
      this.state[`${leafStateKey}Scale`] = 0.2;
    });
  }

  componentDidMount() {
    var opacityDelay = 0.12;
    var opacitySpeed = 0.75;
    var scaleDelay = 0.14;
    var scaleSpeed = 0.9;

    leafStateKeys.forEach((leafStateKey, idx)=> {
      var tmpState = {};
      tmpState[`${leafStateKey}Opacity`] = 1;

      TweenMax.to(this, opacitySpeed, {
        state: tmpState,
        ease: Cubic.easeOut,
        delay: idx * opacityDelay
      });

      tmpState = {};
      tmpState[`${leafStateKey}Scale`] = 1;

      TweenMax.to(this, scaleSpeed, {
        state: tmpState,
        ease: Elastic.easeOut.config(1.2, 0.6),
        delay: idx * scaleDelay
      });
    });
    
    // TweenMax.to(this, 0.75, {
    //   state: {opacity: 0},
    //   ease: Cubic.easeIn,
    //   delay: ((leafStateKeys.length-1) * scaleDelay) + scaleSpeed + 0.25
    // });
  }

  render() {
    return splashJSX.apply(this);
  }
}
