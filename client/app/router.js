import React from 'react';
import ReactDOM from 'react-dom';
import HomePage from './pages/home/index';
import ProfilePage from './pages/profile/index';
import data from './data';

var remote = System._nodeRequire('electron').remote;

class Router {
  constructor() {
    this.el = document.getElementById('app-root');
  }

  login() {
    var myWin = remote.getCurrentWindow();
    myWin.hide();
    myWin.setSize(320, 450);
    myWin.center();
    ReactDOM.render(<HomePage />, this.el);
    myWin.show();
  }

  profile() {
    var myWin = remote.getCurrentWindow();
    myWin.hide();
    myWin.setSize(800, 550);
    myWin.center();
    var props = data.get('profile') || {};

    ReactDOM.render(<ProfilePage {...props}/>, this.el);
    myWin.show();
  }
}

export default new Router();
